using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class TerrainGenerator : MonoBehaviour
{

    public GameObject TerrainHolder;

    public int numberOfActiveTerrains { get { return ActiveTerrains.Count; } }

    public List<GameObject> ActiveTerrains = new List<GameObject>();

    public Dictionary<ObjectArtType, GameObject> TerrainsObjects = new Dictionary<ObjectArtType, GameObject>(); 

    [Tooltip("DIRT > ROCK > ICE > SNOW | Respect the order!")]
    public List<GameObject> Terrains = new List<GameObject>();

    public int InitialAmountToGenerate = 3;

    void Awake()
    {
        if (TerrainHolder == null)
        {
            TerrainHolder = GameObject.FindGameObjectWithTag(Helper.TerrainHolder);
        }
    }

    void Start()
    {
        GetAllTerrains();
        
        CountInitialTerrains();
    }

    //TODO(celso): Move this to pooling
    #region Move this to pooling.
    void GetAllTerrains()
    {
        List<GameObject> temp = GameObject.FindGameObjectsWithTag(Helper.TerrainTag).ToList();

        int dirt = 0;
        int rock = 0;
        int ice = 0;
        int snow = 0;

        foreach (GameObject o in temp)
        {
            TerrainMovement tm = o.GetComponent<TerrainMovement>();

            if (tm.TerrainArtType == ObjectArtType.DIRT)
            {
                //o.gameObject.name = "Terrain_Dirt";
                dirt++;
                //Debug.Log("Found a new terrain!", o.gameObject);
            }
            if (tm.TerrainArtType == ObjectArtType.ROCK)
                rock++;
            if (tm.TerrainArtType == ObjectArtType.ICE)
                ice++;
            if (tm.TerrainArtType == ObjectArtType.SNOW)
                snow++;
        }

        //Debug.Log("Dirt: " + dirt);
        //Debug.Log("Rock: " + rock);
        //Debug.Log("Ice: " + ice);
        //Debug.Log("Snow: " + snow);

    }

    void CountInitialTerrains()
    {
        int dirt = 0;
        int rock = 0;
        int ice = 0;
        int snow = 0;

        foreach (var terrainsObject in TerrainsObjects)
        {
            if (terrainsObject.Key == ObjectArtType.DIRT)
                dirt++;
            if (terrainsObject.Key == ObjectArtType.ICE)
                ice++;
            if (terrainsObject.Key == ObjectArtType.ROCK)
                rock++;
            if (terrainsObject.Key == ObjectArtType.SNOW)
                snow++;
        }
        
        Debug.Log(InitialAmountToGenerate - dirt);
        if (dirt < InitialAmountToGenerate)
            GenerateTerrain(ObjectArtType.DIRT, InitialAmountToGenerate - dirt);
        //if (rock < 3)
        //    GenerateTerrain(ObjectArtType.ROCK, 3 - rock);
        //if (ice < 3)
        //    GenerateTerrain(ObjectArtType.ICE, 3 - ice);
        //if (snow < 3)
        //    GenerateTerrain(ObjectArtType.SNOW, 3 - snow);


    }

    void GenerateTerrain(ObjectArtType terrainObjectArtType, int amountToGenerate = 0)
    {
        //DIRT > ROCK > ICE > SNOW
        switch (terrainObjectArtType)
        {
            case ObjectArtType.DIRT:
                ForGeneration(amountToGenerate, Terrains[0]);
                break;

            case ObjectArtType.ROCK:
                ForGeneration(amountToGenerate, Terrains[1]);
                break;

            case ObjectArtType.ICE:
                ForGeneration(amountToGenerate, Terrains[2]);
                break;

            case ObjectArtType.SNOW:
                ForGeneration(amountToGenerate, Terrains[3]);
                break;
        }        
    }

    void ForGeneration(int x, GameObject go)
    {
        for (int y = 0; y < x; y++)
        {
            Instantiate(go, new Vector3(0, 0, 0), Quaternion.identity);
        }
    }
    #endregion

}
using UnityEngine;
using System.Collections;
using System.Collections.Generic;


[DisallowMultipleComponent]
public class ObstaclesGenerator : MonoBehaviour
{
    
    #region Mono methods
    void Start()
    {
        
    }
    #endregion

    #region Private methods

    /// <summary>
    ///     We want a maximun and a minumun value on the y position,
    /// so we know where we should spawn the terrain. For a initial 
    /// generation it's the only thing we really need to know.
    ///     To reuse the asset we can have the same system from the 
    /// terrain, use a trigger and recycle it to the new position.
    /// For the recycle position we could also use a minimun and maximun
    /// distance between the last obstacle to make a gap but not a huge one.
    /// </summary>
    private void GenerateObstacles()
    {
        
    }
    #endregion
    

}
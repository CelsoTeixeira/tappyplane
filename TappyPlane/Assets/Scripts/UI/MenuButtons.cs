using UnityEngine;
using System.Collections;

[DisallowMultipleComponent]
public class MenuButtons : MonoBehaviour
{
    public GameObject GetReadyButton;

    private GameManager _gameManager;

    void Awake()
    {
        GetReadyButton = GameObject.FindGameObjectWithTag(Helper.GetReadyButton);

        _gameManager = GameObject.FindGameObjectWithTag(Helper.ManagerTag).GetComponent<GameManager>();
    }

    public void StartGame()
    {
        GetReadyButton.SetActive(false);

        _gameManager.ChangeGameState(GameState.WaitingToBegin);
    }
    
    public void LoadScene(int sceneToLoad)
    {
        Application.LoadLevel(sceneToLoad);
    }

    public void TogglePanel(GameObject panelToToggle)
    {
        panelToToggle.SetActive(panelToToggle.activeInHierarchy);
    }

}
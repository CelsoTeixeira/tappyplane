using UnityEngine;
using System.Collections;

public class UIController : MonoBehaviour
{

    public delegate void MenuState();
    public MenuState MyMenuState = () => { };

    public delegate void WaitingToBeginState();
    public WaitingToBeginState MyWaitingToBeginState = () => { };

    public delegate void RunningState();
    public RunningState MyRunningState = () => { };

    public delegate void GameOverState();
    public GameOverState MyGameOverState = () => { } ;

    private GameManager _gameManager;
    
    

    void Awake()
    {
        _gameManager = GameObject.FindGameObjectWithTag(Helper.ManagerTag).GetComponent<GameManager>();
    }

    void Update()
    {
        switch (_gameManager.CurrentGameState)
        {
            case GameState.Menu:
                if (MyMenuState != null) MyMenuState();
                break;

            case GameState.WaitingToBegin:
                if (MyWaitingToBeginState != null) MyWaitingToBeginState();
                break;

            case GameState.Running:
                if (MyRunningState != null) MyRunningState();
                break;

            case GameState.Death:
                
                break;

            case GameState.GameOver:
                if (MyGameOverState != null) MyGameOverState();
                break;
        }
    }
}
using UnityEngine;
using System.Collections;

public class WaitingState : DefaultUIState 
{
    protected override void Awake()
    {
        base.Awake();

        if (UIPanel == null)
        {
            UIPanel = GameObject.FindGameObjectWithTag(Helper.UIWaitingPanel);
        }
        
    }

    protected override void StateControl()
    {

    }

    protected override void OnEnable()
    {
        _uiController.MyWaitingToBeginState += StateControl;
    }

    protected override void OnDisable()
    {
        _uiController.MyWaitingToBeginState -= StateControl;
    }
}
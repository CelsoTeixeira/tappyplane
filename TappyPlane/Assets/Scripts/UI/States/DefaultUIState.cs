using UnityEngine;
using System.Collections;

public class DefaultUIState : MonoBehaviour
{
    public GameObject UIPanel;

    protected UIController _uiController;

    protected virtual void Awake()
    {
        _uiController = GameObject.FindGameObjectWithTag(Helper.ManagerTag).GetComponent<UIController>();
    }

    protected virtual void StateControl() { }
    protected virtual void OnEnable() { }
    protected virtual void OnDisable() { }
}
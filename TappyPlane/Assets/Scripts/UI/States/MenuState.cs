using UnityEngine;
using System.Collections;

public class MenuState : DefaultUIState
{
    private GameManager _gameManager;

    protected override void Awake()
    {
        base.Awake();

        _gameManager = GameObject.FindGameObjectWithTag(Helper.ManagerTag).GetComponent<GameManager>();

        if (UIPanel == null)
        {
            UIPanel = GameObject.FindGameObjectWithTag(Helper.UIMenuPanel);
        }
    }

    protected override void StateControl()
    {

    }
    

    protected override void OnEnable()
    {
        _uiController.MyMenuState += StateControl;
    }

    protected override void OnDisable()
    {
        _uiController.MyMenuState -= StateControl;
    }
    
    
}
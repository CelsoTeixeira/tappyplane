using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// TODO:(celso) We need to create a event system, to allow us to change speed of 
/// TODO:different objects just trigerring this events.
/// </summary>

[DisallowMultipleComponent]
public class ObstaclesManager : MonoBehaviour
{
    public List<ObstacleObject> ActiveObstacles = new List<ObstacleObject>();

    public float MandatoryScrollSpeed = 0.2f;

    public float BackgroundScrollSpeed = 0.5f;

    /// <summary>
    ///     We use this just to broadcast the new scroll speed if we need to change
    /// it at runtime, what we probably will do at some time.
    /// </summary>
    public void ChangeScrollSpeed()
    {
        foreach (ObstacleObject obstacle in ActiveObstacles)
        {
            obstacle.ScrollSpeed = MandatoryScrollSpeed;
        }
    }

    /// <summary>
    ///     A overload to allow us to pass a new scroll speed if it's necessary.
    /// </summary>
    /// <param name="newSpeed"></param>
    public void ChangeScrollSpeed(float newSpeed)
    {
        foreach (ObstacleObject obstacle in ActiveObstacles)
        {
            obstacle.ScrollSpeed = newSpeed;
        }
    }

    public void ZeroScrollSpeed()
    {
        foreach (ObstacleObject obstacle in ActiveObstacles)
        {
            obstacle.ScrollSpeed = 0;
        }
    }

    /// <summary>
    ///     TODO(celso): This need to be tested to see if it's working as intended.
    /// </summary>
    /// <param name="ob"></param>
    public void AddOnActiveObstacles(ObstacleObject ob)
    {
        ActiveObstacles.Add(ob);
    }

    public void RemoveFromActiveObstacles(ObstacleObject ob)
    {
        ActiveObstacles.Remove(ob);
    }

}
using UnityEngine;
using System.Collections;

public class ScoreManager : MonoBehaviour
{
    #region Private 
    private float _scoreCounter = 0f;

    private GameManager _gameManager;
    #endregion

    #region Public / Properties
    public float ScoreCounter { get { return _scoreCounter; } }
    
    public float PointsTimeRatio = 0.5f;
    #endregion

    #region Mono
    private void Awake()
    {
        _gameManager = GameObject.FindGameObjectWithTag(Helper.ManagerTag).GetComponent<GameManager>();
    }
    
    /// <summary>
    ///     Subscribing the ScoreUpdate to the Running Update
    /// from the game manager.
    /// </summary>
    private void OnEnable()
    {
        _gameManager.RunningUpdate += ScoreUpdate;
    }

    /// <summary>
    ///     Un-subscribing the ScoreUpdate method from the Running Update
    /// from the game manager.
    /// </summary>
    private void OnDisable()
    {
        _gameManager.RunningUpdate -= ScoreUpdate;
    }
    #endregion

    #region Public methods
    /// <summary>
    ///     We are going to add this to the Update delegate method on our game manager.
    /// This is what control the points system for our game, the ratio varialbe it's what 
    /// we're going to multiply by to get the final point on each frame.
    /// </summary>
    public void ScoreUpdate()
    {
        _scoreCounter += Time.deltaTime * PointsTimeRatio;
    }

    /// <summary>
    ///     We want to reset the score everytime we enter the game.
    /// </summary>
    public void ScoreReset()
    {
        _scoreCounter = 0f;
    }
    #endregion
}
using System;
using UnityEngine;
using System.Collections;

public enum GameState
{
    None,
    Menu,
    WaitingToBegin,
    Running,
    Death,
    GameOver
}

[DisallowMultipleComponent]
public class GameManager : MonoBehaviour
{
    public GameState _gameState = GameState.Menu;
    public GameState CurrentGameState { get { return _gameState; } }

    //TODO(celso): We should have only two delegates here and a method
    //TODO(celso)  to control subscribe and unsubscribe.
    public delegate void RunningGameState();
    public RunningGameState RunningUpdate = () => { };

    public delegate void RunningFixedGameState();
    public RunningFixedGameState RunningFixedUpdate = () => { };

    public delegate void MenuGameState();
    public MenuGameState MenuUpdate = () => { };

    public delegate void DeathGameState();
    public DeathGameState DeathUpdate = () => { };

    public delegate void GameOverGameState();
    public GameOverGameState GameOverUpdate = () => { };
    
    #region Mono
    private void Awake() { }

    private void Start()
    {
        _gameState = GameState.Menu;        
    }

    /// <summary>
    ///     This is our main game loop. For this to work we need to feed
    /// the right methods into our delegates.
    ///     We're using delegates to simplyfy the loop, but it could be harder
    /// to debug if we need. We really need to document what we have inside each delegate
    /// to avoid stress later.
    /// TODO(celso): We could use only two delegates and subscribe and unsubscribe according with our needs.
    /// </summary>
    private void Update()
    {
        switch (_gameState)
        {
            case GameState.Running:
                if (RunningUpdate != null) RunningUpdate();
                break;
                
            case GameState.Death:
                if (DeathUpdate != null) DeathUpdate();
                break;

            case GameState.GameOver:
                if (GameOverUpdate != null) GameOverUpdate();
                break;

            case GameState.Menu:
                if (MenuUpdate != null) MenuUpdate();
                break;

            case GameState.WaitingToBegin:
                Debug.Log("We're waiting for input to start the game.");
                break;
        }
    }

    void FixedUpdate()
    {
        switch (_gameState)
        {
            case GameState.Running:
                if (RunningFixedUpdate != null) RunningFixedUpdate();
                break;
        }
    }

    #endregion

    /// <summary>
    ///     This is called to change our game state, we could use some validation here, 
    /// but I have no idea what we should check here.  
    /// </summary>
    /// <param name="newState"></param>
    public void ChangeGameState(GameState newState)
    {
        Debug.Log("We're changing states! OLD: " + _gameState + " / NEW: " + newState);
        _gameState = newState;
    }
}
﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ObstacleCollection : MonoBehaviour 
{
    #region Game Obstacle class definition
    /// <summary>
    ///     This is a simple class just to make things a little more simple and clean.
    ///     And with this we have everything we need for our level be generate
    /// inside a single class that stores all the gameObjects we will be using.
    /// </summary>
    [System.Serializable]
    public class GameObstacles
    {
        public ObjectArtType ObstacleArtType;
        public GameObject ObstacleGameObject;

        //Constructor
        public GameObstacles(ObjectArtType artType, GameObject newGo)
        {
            ObstacleArtType = artType;
            ObstacleGameObject = newGo;
        }
    }
    #endregion

    public Dictionary<ObjectArtType, GameObstacles> Collection = 
                    new Dictionary<ObjectArtType, GameObstacles>();

    public GameObject GetObstacle(ObjectArtType objectArtTypeToCheck)
    {
        if (Collection.ContainsKey(objectArtTypeToCheck))
        {
            return Collection[objectArtTypeToCheck].ObstacleGameObject;
        }
        else
        {
            return null;
        }
    }

    public bool CheckForKey(ObjectArtType objectArtTypeToCheck)
    {
        return Collection.ContainsKey(objectArtTypeToCheck);
    }
}
﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TerrainCollection : MonoBehaviour 
{
    #region Game Terrain class definition
    /// <summary>
    ///     This is a simple class just to make things a little more simple and clean.
    ///     And with this we have everything we need for our level be generate
    /// inside a single class that stores all the gameObjects we will be using.
    /// </summary>
    [System.Serializable]
    public class GameTerrain
    {
        public ObjectArtType TerrainArtType;
        public GameObject TerrainGameObject;

        //Constructor
        public GameTerrain(ObjectArtType artType, GameObject newGo)
        {
            TerrainArtType = artType;
            TerrainGameObject = newGo;
        }
    }
    #endregion

    public Dictionary<ObjectArtType, GameTerrain> Collection = new Dictionary<ObjectArtType, GameTerrain>();

    public GameObject GetObstacle(ObjectArtType objectArtTypeToCheck)
    {
        if (Collection.ContainsKey(objectArtTypeToCheck))
        {
            return Collection[objectArtTypeToCheck].TerrainGameObject;
        }
        else
        {
            return null;
        }
    }

    public bool CheckForKey(ObjectArtType objectArtTypeToCheck)
    {
        return Collection.ContainsKey(objectArtTypeToCheck);
    }
}
using UnityEngine;
using System.Collections;

public static class Helper
{
    public static string ManagerTag = "Manager";
    public static string PlayerTag = "Player";

    public static string EndZoneTag = "EndZone";
    public static string BeginZoneTag = "BeginZone";

    public static string TerrainTag = "Terrain";
    public static string ObstacleTag = "Obstacle";

    public static string GetReadyButton = "GetReady";

    
    public static string ObjectsHolder = "ObjectsHolder";
    public static string TerrainHolder = "TerrainHolder";
    public static string ObstacleHolder = "ObstaclesHolder";
    public static string PoolsHolder = "PoolsHolder";

    public static string UIWaitingPanel = "UIWaitingPanel";
    public static string UIMenuPanel = "UIMenuPanel";
    public static string UIRunningPanel = "UIRunningPanel";
    public static string UIGameOverPanel = "UIWaitingPanel";
    
}
using UnityEngine;
using System.Collections;

/// <summary>
///     The first thing we want to load it's all the function components.
/// We could load everything here and cache on the same moment. We could also
/// get the components from here and not try to get it from every place we need.
/// This could minimize stress?
/// </summary>

[DisallowMultipleComponent]
public class Loader : MonoBehaviour
{
    public GameObject ManagerObject;

    public GameManager GameManager;

    public ObstaclesManager ObstaclesManager;
    public ObstaclesGenerator ObstaclesGenerator;
    
    public TerrainGenerator TerrainGenerator;

    public ObjectPoolingManager PoolingManager;

    
    public GameObject ObjectsHolder;
    public GameObject TerrainHolder;
    public GameObject ObstaclesHolder;
    public GameObject PoolsHolder;

    #region Mono

    public void Awake()
    {
        ObjectsHolder = GameObject.FindGameObjectWithTag(Helper.ObjectsHolder);

        if (ObjectsHolder == null)
        {
            GameObject newHolder = new GameObject("ObjectsHolder");
            newHolder.tag = Helper.ObjectsHolder;

            ObjectsHolder = newHolder;
        }

        TerrainHolder = GameObject.FindGameObjectWithTag(Helper.TerrainHolder);

        if (TerrainHolder == null)
        {
            GameObject newTerrainHolder = new GameObject("TerrainHolder");
            newTerrainHolder.tag = Helper.TerrainHolder;
            newTerrainHolder.transform.SetParent(ObjectsHolder.transform);

            TerrainHolder = newTerrainHolder;
        }

        ObstaclesHolder = GameObject.FindGameObjectWithTag(Helper.ObstacleHolder);

        if (ObstaclesHolder == null)
        {
            GameObject newObstacleHolder = new GameObject("ObstaclesHolder");
            newObstacleHolder.tag = Helper.ObstacleHolder;
            newObstacleHolder.transform.SetParent(ObjectsHolder.transform);

            ObstaclesHolder = newObstacleHolder;
        }

        PoolsHolder = GameObject.FindGameObjectWithTag(Helper.PoolsHolder);

        if (PoolsHolder == null)
        {
            GameObject newPoolsHolder = new GameObject("PoolsHolder");
            newPoolsHolder.tag = Helper.PoolsHolder;
            newPoolsHolder.transform.SetParent(ObjectsHolder.transform);

            PoolsHolder = newPoolsHolder;
        }

        ManagerObject = GameObject.FindGameObjectWithTag(Helper.ManagerTag);

        if (ManagerObject == null)
        {
            GameObject managerObject = new GameObject("Manager");
            managerObject.tag = Helper.ManagerTag;

            ManagerObject = managerObject;
            
            managerObject.AddComponent<GameManager>();

            managerObject.AddComponent<ObstaclesManager>();
            managerObject.AddComponent<ObstaclesGenerator>();

            managerObject.AddComponent<TerrainGenerator>();

            managerObject.AddComponent<ObjectPoolingManager>();

        }
        GameManager = ManagerObject.GetComponent<GameManager>();

        ObstaclesManager = ManagerObject.GetComponent<ObstaclesManager>();
        ObstaclesGenerator = ManagerObject.GetComponent<ObstaclesGenerator>();
        
        TerrainGenerator = ManagerObject.GetComponent<TerrainGenerator>();

        PoolingManager = ManagerObject.GetComponent<ObjectPoolingManager>();
    }

    #endregion
    

}
﻿using UnityEngine;
using System.Collections;

public class TerrainType
{
    public ObjectArtType ObjectArtType;
    public GameObject Terrain;

    public TerrainType(ObjectArtType newObjectArtType, GameObject obj)
    {
        ObjectArtType = newObjectArtType;
        Terrain = obj;
    }
}
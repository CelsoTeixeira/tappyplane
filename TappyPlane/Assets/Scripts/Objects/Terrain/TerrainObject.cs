using UnityEngine;
using System.Collections;

public class TerrainObject : ObjectsBase
{
    public float ScrollSpeed = 0.7f;
    
    private float _width;
    private PolygonCollider2D _collider;
    
    protected override void Awake()
    {
        base.Awake();

        _collider = GetComponent<PolygonCollider2D>();

        //Make sure we have the right tag.
        gameObject.tag = Helper.TerrainTag;
        
    }

    protected override void Start()
    {
        //We get the holder for the current object.
        ObjectHolder = GameObject.FindGameObjectWithTag(Helper.TerrainHolder);

        base.Start();

        //The width of our collider to make sure the math when we loop the object it's right.
        _width = _collider.bounds.size.x;

        //We parent the gameObject to the right holder.
        this.gameObject.transform.SetParent(ObjectHolder.transform);

        _collider.isTrigger = true;

    }

    protected override void ObjectUpdate()
    {
        float newX = ScrollSpeed * Time.deltaTime;
        Vector3 newPos = new Vector3(newX, 0f, 0f);

        transform.position -= newPos;
    }

    protected override void OnTriggerEnter2D(Collider2D collision)
    {

    }

    protected override void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag(Helper.EndZoneTag))
        {
            //_triggerPosition.x = (_width * _terrainGenerator.numberOfActiveTerrains - 1) - _width;

        }
    }

    protected override void OnEnable()
    {
        GameManager.RunningUpdate += this.ObjectUpdate;
    }

    protected override void OnDisable()
    {
        GameManager.RunningUpdate -= this.ObjectUpdate;
    }
}
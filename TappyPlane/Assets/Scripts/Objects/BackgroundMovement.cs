using UnityEngine;
using System.Collections;

public class BackgroundMovement : MonoBehaviour
{
    [Tooltip("The texture offset speed;")]
    public float ScrollSpeed = 0.5f;

    private Renderer _renderer;
    private GameManager _gameManager;
    private ObstaclesManager _obstaclesManager;

    private float _offset;

    private float _currentTime = 0;

    #region Mono methods
    void Awake()
    {
        _gameManager = GameObject.FindGameObjectWithTag(Helper.ManagerTag).GetComponent<GameManager>();
        _obstaclesManager = GameObject.FindGameObjectWithTag(Helper.ManagerTag).GetComponent<ObstaclesManager>();
        _renderer = GetComponent<Renderer>();
    }

    void OnEnable()
    {
        _offset = 0;
        _currentTime = 0;
        _renderer.material.SetTextureOffset("_MainTex", new Vector2(_offset, 0));

        _gameManager.RunningUpdate += BackgroundUpdate;
    }

    void OnDisable()
    {
        _gameManager.RunningUpdate -= BackgroundUpdate;
    }
    #endregion

    void BackgroundUpdate()
    {
        _currentTime += Time.deltaTime;
        
        _offset = _currentTime * ScrollSpeed;

        _renderer.material.SetTextureOffset("_MainTex", new Vector2(_offset, 0));

    }

}
using UnityEngine;
using System.Collections;

public class TerrainMovement : MonoBehaviour
{
    public ObjectArtType TerrainArtType;

    public float ScrollSpeed = 0.7f;

    private TerrainGenerator _terrainGenerator;

    private float _width;
    private PolygonCollider2D _polygonCollider2D;

    private Vector3 _triggerPosition = Vector3.zero;

    private GameManager _gameManager;

    void Awake()
    {
        _terrainGenerator = GameObject.FindGameObjectWithTag
        (Helper.ManagerTag).GetComponent<TerrainGenerator>();

        _polygonCollider2D = GetComponent<PolygonCollider2D>();

        _gameManager = GameObject.FindGameObjectWithTag
          (Helper.ManagerTag).GetComponent<GameManager>();

        //If we dont have a entry on the collection we add one for future reference.
        //if (!_terrainGenerator.CheckForKey(TerrainArtType))
        //{
        //    _terrainGenerator.TerrainCollection.Add(TerrainArtType, new TerrainGenerator.TerrainArtType(
        //                                                            TerrainArtType, this.gameObject));
        //}
    }
    
    void Start()
    {
        //This is the size of our collider.
        _width = _polygonCollider2D.bounds.size.x;

        //We save our default Y and Z
        _triggerPosition.y = transform.position.y;
        _triggerPosition.z = transform.position.z;

        this.transform.parent = _terrainGenerator.TerrainHolder.transform;      //We parent this object to our holder
        //Debug.Log("Width = " + _width);
    }

    /// <summary>
    ///     Simply update to make it scroll the object
    /// on the x position.
    /// </summary>
    void TerrainMovementUpdate()
    {
        float newX = ScrollSpeed * Time.deltaTime;
        Vector3 newVector = new Vector3(newX, 0, 0);

        transform.position -= newVector;
    }

    void OnEnable()
    {
        _terrainGenerator.ActiveTerrains.Add(this.gameObject);

        _gameManager.RunningUpdate += TerrainMovementUpdate;
    }

    void OnDisable()
    {
        _terrainGenerator.ActiveTerrains.Remove(this.gameObject);

        _gameManager.RunningUpdate -= TerrainMovementUpdate;
    }

    /// <summary>
    ///     If we trigger exit an collider with the EndZone tag
    /// we want to reposition our object based on the amount of
    /// active objects and the size, so we have a loop.
    /// </summary>
    /// <param name="collision"></param>
    void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag(Helper.EndZoneTag))
        {
            _triggerPosition.x = (_width * _terrainGenerator.numberOfActiveTerrains - 1) - _width;
            
            //Debug.Log("Trigger Position: " + _triggerPosition.x);

            transform.position = _triggerPosition;
        }
    }
}
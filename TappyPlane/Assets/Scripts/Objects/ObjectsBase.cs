using UnityEngine;
using System.Collections;

public enum ObjectArtType {NONE, DIRT, ROCK, ICE, SNOW }
public enum ObjectType { NONE, TERRAIN, OBSTACLE}

[DisallowMultipleComponent]
public class ObjectsBase : MonoBehaviour 
{
    public ObjectDefinition Definition = new ObjectDefinition();
    
    [Tooltip("The object itself.")]
    public GameObject ObjectToPool;

    [Tooltip("The holder for the object")]
    public GameObject ObjectHolder;

    public ObjectPooling Pool;

    protected ObjectPoolingManager PoolManager;

    protected GameManager GameManager;

    protected virtual void Awake()
    {
        ObjectToPool = this.gameObject;

        PoolManager = GameObject.FindGameObjectWithTag(Helper.ManagerTag).GetComponent<ObjectPoolingManager>();
        GameManager = GameObject.FindGameObjectWithTag(Helper.ManagerTag).GetComponent<GameManager>();
    }

    protected virtual void Start()
    {
        
        CheckForPoolValue();
        
        Pool.AddOnList(this.gameObject);
    }

    protected virtual void ObjectUpdate() { }

    protected virtual void OnTriggerEnter2D(Collider2D collision) { }
    protected virtual void OnTriggerExit2D(Collider2D collision) { }
        
    protected virtual void OnEnable() { }
    protected virtual void OnDisable() { }

    public void SetupPosition(Vector3 newPos)
    {
        gameObject.transform.position = newPos;
    }

    /// <summary>
    ///     We get or make our reference for the right pool.
    /// </summary>
    protected void CheckForPoolValue()
    {
        //Check for a pool on the collection
        if (PoolManager.Collection.CheckForKey(Definition))
        {
            //If we have one, we get the reference.
            Pool = PoolManager.Collection.GetValue(Definition);
        }
        else if (!PoolManager.Collection.CheckForKey(Definition))
        {
            Debug.Log("Creating a new pool component.");

            //If we don't have one we create a new Component
            //and add the reference to the collection
            Pool = PoolManager.AddNewPoolComponent(Definition, ObjectToPool, ObjectHolder);
        }

    }
}
using UnityEngine;
using System.Collections;

[System.Serializable]
public class ObjectDefinition 
{
    [SerializeField]
    public ObjectArtType ArtType = ObjectArtType.NONE;

    [SerializeField]
    public ObjectType Type = ObjectType.NONE;

    public ObjectDefinition()
    {
        
    }

    public ObjectDefinition(ObjectArtType artType, ObjectType type)
    {
        ArtType = artType;
        Type = type;
    }
}
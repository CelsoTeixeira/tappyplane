using UnityEngine;
using System.Collections;

public class ObstacleObject : ObjectsBase
{
    public float ScrollSpeed = 0.2f;

    private ObstaclesManager _obstaclesManager;

    private PolygonCollider2D _collider;
    
    protected override void Awake()
    {
        base.Awake();

        _obstaclesManager = GameObject.FindGameObjectWithTag(Helper.ManagerTag).GetComponent<ObstaclesManager>();

        _collider = GetComponent<PolygonCollider2D>();

        gameObject.tag = Helper.ObstacleTag;
    }
    
    protected override void Start()
    {
        ObjectHolder = GameObject.FindGameObjectWithTag(Helper.ObstacleHolder);

        base.Start();
        
        ScrollSpeed = _obstaclesManager.MandatoryScrollSpeed;

<<<<<<< HEAD
        this.gameObject.tag = Helper.ObstacleTag;
=======
        this.transform.SetParent(ObjectHolder.transform);
        
        _collider.isTrigger = true;
>>>>>>> cfbb6282efa998183dfdb70cba1d27f4d62fc470
    }

    /// <summary>
    ///     We create a new Vector3 with the same Y and Z values 
    /// but we get a new value for X, we decrease the transform
    /// from the game object by the new vector to make it move 
    /// backwards on the X axis.
    /// </summary>
    protected override void ObjectUpdate()
    {
        //Move every frame by the scroll speed.
        float newX = ScrollSpeed * Time.deltaTime;
        Vector3 newPos = new Vector3(newX, 0f, 0f);

        transform.position -= newPos;
    }

    /// <summary>
    ///     When we enter the trigger.
    ///         If the tag its the Player we do the following steps:
    ///             ->Change the speed of the scroll.
    ///             ->Change the game state for death.
    /// </summary>
    /// <param name="collision"></param>
    protected override void OnTriggerEnter2D(Collider2D collision)
    {
        //If its the player
        if (collision.gameObject.CompareTag(Helper.PlayerTag))
        {
            //we stop all the velocity.
            _obstaclesManager.ZeroScrollSpeed();

            //We change the state to death.
           GameManager.ChangeGameState(GameState.Death);
        }
    }

    /// <summary>
    ///     When we exit the trigger.
    ///         If the tag its the EndZoneTag we do the following steps:
    ///             ->Add this on the Queue on the respective Pool.
    ///             ->Deactive the gameObject.
    /// </summary>
    /// <param name="collision"></param>
    protected override void OnTriggerExit2D(Collider2D collision)
    {
        //If we are leaving the game area
        if (collision.gameObject.CompareTag(Helper.EndZoneTag))
        {
            //we want to move the object to the queue.
            Pool.AddOnQueue(this);

            //we want to deactive the gameobject.
            this.gameObject.SetActive(false);
        }
    }

    /// <summary>
    ///     When we active the object we do the following:
    ///         ->We add to the obstacle tracker.
    ///         ->We add the method to the Running State on Game Manager.
    ///         ->We make sure we have the right ScrollSpeed.
    /// </summary>
    protected override void OnEnable()
    {
        //Make sure we're tracking the active objects for whatever reasons.
        _obstaclesManager.AddOnActiveObstacles(this);

        //Add our method to the main loop.
        GameManager.RunningUpdate += ObjectUpdate;

        //Make sure we have the right scroll speed.
        ScrollSpeed = _obstaclesManager.MandatoryScrollSpeed;
    }

    /// <summary>
    ///     When we disable the object we do the following:
    ///         ->Remove from the tracker.
    ///         ->Remove the method from the Running State on Game Manager.
    /// </summary>
    protected override void OnDisable()
    {
        //Remove from our track.
        _obstaclesManager.RemoveFromActiveObstacles(this);

        //Remove our method from the delegate.
        GameManager.RunningUpdate -= ObjectUpdate;
    }
}
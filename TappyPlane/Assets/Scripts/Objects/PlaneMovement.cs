using UnityEngine;
using System.Collections;

/// <summary>
///     This is our player movement controller.
/// We're doing an fake physics here just to have more control
/// on what we affect and how we affect the plane. 
/// We added an rigidbody to make sure we are on the collision 
/// loop.
/// </summary>

[DisallowMultipleComponent]
[RequireComponent(typeof(Rigidbody2D))]
public class PlaneMovement : MonoBehaviour
{    
    public Vector3 Gravity;     //This is our gravity, we apply this to the velocity.
    public Vector3 ImpulseVelocity;     //This is the velocity we apply when we tap the screen.
    public float MaxSpeed = 5f;     //The max velocity we're going to allow the plane to have.

    public float ForwardSpeed = 1f;    //We're not going to use this, we're moving the objects and not the plane.

    private Vector3 _velocity = Vector3.zero;   //Our velocity track.

    private bool didImpulse = false;        //If we tap or not the screen and should apply an impulse.

    private Rigidbody2D _rigidbody2D;
    private GameManager _gameManager;

    #region Mono
    void Awake()
    {
        _rigidbody2D = GetComponent<Rigidbody2D>();

        _gameManager = GameObject.FindGameObjectWithTag(Helper.ManagerTag).GetComponent<GameManager>();
    }

    /// <summary>
    ///     We're making sure we're Kinematic and we 
    /// have the interpolation on to smooth our rotation
    /// on the rigidoby.
    /// </summary>
    void Start()
    {
        _rigidbody2D.isKinematic = true;        //We're excluding this from the physics loop
        _rigidbody2D.interpolation = RigidbodyInterpolation2D.Interpolate;      //We're marking this to interpolate if necessary.
    }

    void OnEnable()
    {
        _gameManager.RunningUpdate += PlayerUpdate;
        _gameManager.RunningFixedUpdate += PlayerFixedUpdate;
    }

    void OnDisable()
    {
        _gameManager.RunningUpdate -= PlayerUpdate;
        _gameManager.RunningFixedUpdate -= PlayerFixedUpdate;
    }
    #endregion

    /// <summary>
    ///     Inputs and graphics update are deal here.
    ///     If we registry an Input.Space, or a mouse/touch
    /// we want to do a impulse. The impulse will be added inside
    /// the FixedUpdate.
    /// </summary>
    void PlayerUpdate()
    {
        if (Input.GetKeyDown(KeyCode.Space) || Input.GetMouseButtonDown(0))
        {
            didImpulse = true;
        }
    }

    /// <summary>
    ///     We're dealing with physics on the FixedUpdate,
    /// every physics entity should be moved to the FixedUpdate.
    /// We're leaving Update to deal with grapichs and input.
    ///     We have moved to use rigidbody and not deal with 
    /// moving the plane directly. We now can registry collisions.
    ///     
    ///     So, related to the movement:
    ///         -We make the x velocity be equal to our ForwardSpeed, 
    /// right now, our logic it's using this because our game will 
    /// make the other objects to move based on a velocity.
    ///         -We add the gravity times the delta time.
    ///         -We check if we need to add Impulse or not,
    /// If we need to add impulse, the first thing it's to reset the
    /// y velocity and only then we add the ImpulseVelocity 
    /// to the velocity.
    ///         -Clamp the velocity to a maximun.
    ///         -Make the rigidbody velocity be our calculated velocity.
    /// 
    ///     Related to the rotation:
    ///         -Our new angle will be a lerp from 0 to -45, 
    /// based on our current velocity by the MaxSpeed.
    ///         -After we have the angle, we apply it to the
    /// plane with the rigidbody MoveRotation method just to 
    /// have a smooth interpolation.
    /// </summary>
    void PlayerFixedUpdate()
    {
        _velocity.x = ForwardSpeed;

        _velocity += Gravity * Time.fixedDeltaTime;
        
        if (didImpulse)
        {
            didImpulse = false;

            if (_velocity.y < 0)
                _velocity.y = 0;

            _velocity += ImpulseVelocity;
        }

        _velocity = Vector3.ClampMagnitude(_velocity, MaxSpeed);

        //transform.position += _velocity * Time.deltaTime;
        _rigidbody2D.velocity = _velocity;

        float angle = Mathf.LerpAngle(0, -90, -_rigidbody2D.velocity.y / MaxSpeed);

        //float angle = 0;
        //if (_velocity.y < 0)
        //{
        //    angle = Mathf.Lerp(0, -90, -_velocity.y/MaxSpeed);

        //}
        
        _rigidbody2D.MoveRotation(angle);

        //transform.rotation = Quaternion.Euler(0, 0, angle);

        //Debug.Log("Rigidbody Velocity: " + _rigidbody2D.velocity);
    }
}
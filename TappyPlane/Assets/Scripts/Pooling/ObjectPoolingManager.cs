using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ObjectPoolingManager : MonoBehaviour
{
    [Tooltip("This is the our collection of pools.")]
    [SerializeField]
    public ObjectPoolingCollection Collection = new ObjectPoolingCollection();

    public GameObject PoolsHolder;
    
    void Awake()
    {
        if(PoolsHolder == null)
            PoolsHolder = GameObject.FindGameObjectWithTag(Helper.PoolsHolder);
    }

    public ObjectPooling AddNewPoolComponent(ObjectDefinition def, GameObject objToPool, GameObject objHolder)
    {
        Debug.Log("POOLING MANAER->Adding a new pool component.");

        //Create the component
        ObjectPooling newPool = PoolsHolder.AddComponent<ObjectPooling>();
        newPool.SetupPool(objToPool, def.ArtType, def.Type, objHolder);

        //Store it on the collection
        Collection.StoreValue(def, newPool);

        //return it
        return newPool;
    }
}
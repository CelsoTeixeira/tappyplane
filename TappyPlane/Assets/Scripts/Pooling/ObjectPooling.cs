using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ObjectPooling : MonoBehaviour
{
    public GameObject ObjectToPool;

    public ObjectDefinition Definition = new ObjectDefinition();
    
    public GameObject ObjectHolder;

    //We will track all the GameObject on the pool.
    public List<GameObject> PoolObjects = new List<GameObject>();
    
    public Queue<ObjectsBase> PoolQueue = new Queue<ObjectsBase>();

    public int GetPoolCount { get { return PoolQueue.Count; } }

    /// <summary>  TODO: Test this method
    ///     Get the first object on the Queue, 
    /// if we dont have one we create it, set the parent
    /// to be inside the holders and return it.
    /// </summary>
    public GameObject GetFirstOnQueue(ObjectType def)
    {
        if (PoolQueue.Count == 0 || PoolQueue == null)
        {

            GameObject newPoolObject = Instantiate(ObjectToPool, Vector3.zero, Quaternion.identity) as GameObject;
            newPoolObject.transform.SetParent(ObjectHolder.transform);

            //ObjectsBase newObjectsBase = newPoolObject.GetComponent<ObjectsBase>();
            //newObjectsBase.Definition = Definition;
            //newObjectsBase.ObjectToPool = ObjectToPool;
            //newObjectsBase.ObjectHolder = ObjectHolder;

            return newPoolObject;
        }
        else
        {
            return PoolQueue.Dequeue().gameObject;
        }
    }
    
    /// <summary>
    ///     Add a specific GameObject to the Pool Queue.
    /// </summary>
    /// <param name="go"></param> Object to be added to the Queue.
    public void AddOnQueue(ObjectsBase go)
    {
        //Debug.Log("Pool Enqueue: " + go + " / Pool count: " + PoolQueue.Count, go);
        PoolQueue.Enqueue(go);
    }

    public void AddOnList(GameObject obj)
    {
        PoolObjects.Add(obj);
    }

    /// <summary>
    ///     Setup our new pool and what we need to make
    /// it functional.
    /// </summary>
    /// <param name="newGo"></param> ObjectToPool
    /// <param name="newArtType"></param> PoolArtType {DIRT, ICE, SNOW ... }
    /// <param name="newHolder"></param> Holder to group eveything
    public void SetupPool(GameObject newGo, ObjectArtType newArtType, ObjectType newType, GameObject newHolder)
    {
        ObjectToPool = newGo;
        Definition.ArtType = newArtType;
        Definition.Type = newType;
        ObjectHolder = newHolder;
    }
}

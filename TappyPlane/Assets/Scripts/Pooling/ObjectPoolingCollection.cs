using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

//TODO: Clean debug.

[System.Serializable]
public class ObjectPoolingCollection 
{
    [SerializeField]
    public Dictionary<ObjectDefinition, ObjectPooling> PoolCollection = new Dictionary<ObjectDefinition, ObjectPooling>(); 
    
    public int CollectionCount()
    {
        return PoolCollection.Count;
    }
    
    /// <summary>
    ///     Check for a value inside the pool collection using the 
    /// ObjectDefinition as the key.
    /// </summary>
    /// <param name="def"></param>  ObjectDefinition
    /// <returns></returns> bool based if we have the value or not.
    public bool CheckForKey(ObjectDefinition def)
    {
        Debug.Log("Cheking the key-> " + def.ArtType + " / " + def.Type);

        foreach (var objectPooling in PoolCollection)
        {
            if (objectPooling.Key.ArtType == def.ArtType && objectPooling.Key.Type == def.Type)
            {
                Debug.Log("We found a equal on the Pooling Collection");
                return true;
            }
        }

        Debug.Log("We didnt found a equal on the Pooling Collection");
        return false;
    }
    
    /// <summary>
    ///     Check for a value inside the pool collection using the 
    /// ObjectDefinition as the key and return if we have a match.
    /// </summary>
    /// <param name="def"></param>  ObjectDefinition
    /// <returns></returns> Return the ObjectPooling from the collection.
    public ObjectPooling GetValue(ObjectDefinition def)
    {
        if (CheckForKey(def))
        {
            for (int x = 0; x < PoolCollection.Count; x++)
            {
                if (def.ArtType == PoolCollection.ToArray()[x].Key.ArtType &&
                    def.Type == PoolCollection.ToArray()[x].Key.Type)
                {
                    return PoolCollection.ToArray()[x].Value;
                }
            }
        }
        
        Debug.Log("POOLING COLLECTION.We don't have a value for the respective key.");
        return null;
    }

    /// <summary>
    ///     Use this to store a new pool with a reference to the Object
    /// definition as the dictionary key.
    /// </summary>
    /// <param name="def"></param>  Dictionary (pooling collection) key.
    /// <param name="pool"></param> Dictionary (pooling collection) value.
    public void StoreValue(ObjectDefinition def, ObjectPooling pool)
    {
        if (!CheckForKey(def))
        {
            //Debug.Log("Adding a new value on the collection.");
            //Debug.Log("Key: " + def.ArtType + " / " + def.Type);
            
            PoolCollection.Add(def, pool);
            
        }
        //else
        //{
        //    Debug.Log("We already have the value.");
        //}
    }
}
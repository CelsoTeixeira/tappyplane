using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class DebugPoolingSystem : MonoBehaviour
{
    public ObjectPoolingManager PoolingManager;
    
    void Awake()
    {
        PoolingManager = GameObject.FindGameObjectWithTag(Helper.ManagerTag).GetComponent<ObjectPoolingManager>();
    }

    public void AddNewPool()
    {
        GameObject temp1 = new GameObject();
        temp1.name = "Temp1";
        GameObject temp2 = new GameObject();
        temp2.name = "Temp2";

        ObjectDefinition newDef = new ObjectDefinition(ObjectArtType.SNOW, ObjectType.TERRAIN);

        ObjectPooling newpo = PoolingManager.AddNewPoolComponent(newDef, temp1, temp2);
    }

    public void AddNewPool2()
    {
        GameObject temp1 = new GameObject();
        temp1.name = "Temp1";
        GameObject temp2 = new GameObject();
        temp2.name = "Temp2";

        ObjectDefinition newDef = new ObjectDefinition(ObjectArtType.ICE, ObjectType.OBSTACLE);

        ObjectPooling newpo = PoolingManager.AddNewPoolComponent(newDef, temp1, temp2);
    }

    public void CollectionCount()
    { 
        Debug.Log("Collection count: " + PoolingManager.Collection.CollectionCount());
    }

    public void GetCollectionValues()
    {
        foreach (var pooling in PoolingManager.Collection.PoolCollection)
        {
            Debug.Log("ArtType: " + pooling.Key.ArtType + " / Type: " + pooling.Key.Type, pooling.Value.gameObject);
        }
    }
}